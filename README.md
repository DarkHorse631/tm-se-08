## SE-08

    SE-08 is a task manger program.


## DEVELOPER

    Grishin Vitaliy
    darkhorse63189@gmail.com

 ## SOFTWARE REQUIREMENTS

    JDK 1.7 - 10

 ## TECHNOLOGY STACK

    Java 1.8
    JUnit 4.11
    Maven 4.0

 ## USING THE PROJECT MANAGER
    From the command-line

    Download the project and run it with:

    java -jar C:\Users\user\IdeaProjects\tm-se-03\tm-se-08\target\task-manager-1.0.0.jar

 ## BUILDING FROM SOURCE

    mvn install