package ru.grishin.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.api.repository.UserRepository;
import ru.grishin.tm.api.service.UserService;
import ru.grishin.tm.entity.User;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.util.HashUtil;

import java.util.Collection;
import java.util.UUID;

public final class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void registryUser(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (userRepository.findLogin(login) != null) return;
        final User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setLogin(login);
        user.setPassword(HashUtil.PASSWORD_TO_HASH(password));
        user.setRoleType(RoleType.USER);
        userRepository.persist(user);
    }

    @Override
    public void registryAdmin(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (userRepository.findLogin(login) != null) return;
        final User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setLogin(login);
        user.setPassword(HashUtil.PASSWORD_TO_HASH(password));
        user.setRoleType(RoleType.ADMIN);
        userRepository.persist(user);
    }

    @Nullable
    @Override
    public User login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        final User user = userRepository.findLogin(login);
        if (user.getPassword().equals(HashUtil.PASSWORD_TO_HASH(password))) return user;
        return null;
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        if (userRepository.isExist(id)) userRepository.remove(id);
    }

    @Override
    public void update(@Nullable final String id, @Nullable final String login, @Nullable final String password, @Nullable final RoleType roleType) {
        if (id == null || id.isEmpty()) return;
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (roleType == null) return;
        if (userRepository.isExist(id))
            userRepository.update(id, login, HashUtil.PASSWORD_TO_HASH(password), roleType);
    }

    @Override
    public void updatePassword(@Nullable final String id, @Nullable final String currentPassword, @Nullable final String newPassword) {
        if (id == null || id.isEmpty()) return;
        if (currentPassword == null || currentPassword.isEmpty()) return;
        if (newPassword == null || newPassword.isEmpty()) return;
        if (!userRepository.isExist(id)) return;
        if (HashUtil.PASSWORD_TO_HASH(currentPassword).equals(userRepository.findOne(id).getPassword()))
            userRepository.updatePassword(id, HashUtil.PASSWORD_TO_HASH(newPassword));
    }

    @Override
    public void merge(@Nullable final User user) {
        if (user.getId() == null || user.getId().isEmpty()) return;
        userRepository.merge(user);
    }

    @Nullable
    @Override
    public Collection<User> findAll() {
        return userRepository.findAll();
    }

    @Nullable
    @Override
    public User findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return userRepository.findOne(id);
    }
}
