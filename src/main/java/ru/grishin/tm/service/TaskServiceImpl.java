package ru.grishin.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.api.repository.TaskRepository;
import ru.grishin.tm.api.service.TaskService;
import ru.grishin.tm.entity.Task;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

public final class TaskServiceImpl implements TaskService {
    private final TaskRepository taskRepository;

    public TaskServiceImpl(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(@Nullable final String projectId, @Nullable final String userId, @Nullable final String name, @Nullable final String description, @Nullable final Date dateStart, @Nullable final Date dateFinish) {
        if (projectId == null || projectId.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        if (dateStart == null || dateStart.toString().isEmpty()) return;
        if (dateFinish == null || dateFinish.toString().isEmpty()) return;
        final Task task = new Task();
        task.setProjectId(projectId);
        task.setUserId(userId);
        task.setId(UUID.randomUUID().toString());
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(dateStart);
        task.setDateFinish(dateFinish);
        taskRepository.persist(task);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return;
        if (id == null || id.isEmpty()) return;
        taskRepository.remove(userId, id);
    }

    @Override
    public void update(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description, @Nullable final Date dateStart, @Nullable final Date dateFinish) {
        if (userId == null || userId.isEmpty()) return;
        if (id == null || id.isEmpty()) return;
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        if (dateStart == null || dateStart.toString().isEmpty()) return;
        if (dateFinish == null || dateFinish.toString().isEmpty()) return;
        if (taskRepository.isExist(id))
            taskRepository.update(userId, id, name, description, dateStart, dateFinish);
    }

    @Override
    public void merge(@Nullable final Task task) {
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) return;
        if (task.getUserId() == null || task.getUserId().isEmpty()) return;
        if (task.getId() == null || task.getId().isEmpty()) return;
        taskRepository.merge(task);
    }

    @Nullable
    @Override
    public Collection<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return taskRepository.findAll(userId);
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findOne(userId, id);
    }

    @Override
    public void deleteByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        taskRepository.deleteByProjectId(userId, projectId);
    }

}
