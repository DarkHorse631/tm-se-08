package ru.grishin.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.api.repository.ProjectRepository;
import ru.grishin.tm.api.service.ProjectService;
import ru.grishin.tm.entity.Project;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

public final class ProjectServiceImpl implements ProjectService {
    private final ProjectRepository projectRepository;

    public ProjectServiceImpl(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description, @Nullable final Date dateStart, @Nullable final Date dateFinish) {
        if (userId == null || userId.isEmpty()) return;
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        if (dateStart == null || dateStart.toString().isEmpty()) return;
        if (dateFinish == null || dateFinish.toString().isEmpty()) return;
        final Project project = new Project();
        project.setId(UUID.randomUUID().toString());
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(dateStart);
        project.setDateFinish(dateFinish);
        projectRepository.persist(project);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return;
        if (id == null || id.isEmpty()) return;
        projectRepository.remove(userId, id);
    }

    @Override
    public void update(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description, @Nullable final Date dateStart, @Nullable final Date dateFinish) {
        if (userId == null || userId.isEmpty()) return;
        if (id == null || id.isEmpty()) return;
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        if (dateStart == null || dateStart.toString().isEmpty()) return;
        if (dateFinish == null || dateFinish.toString().isEmpty()) return;
        if (projectRepository.isExist(id))
            projectRepository.update(userId, id, name, description, dateStart, dateFinish);
    }

    @Override
    public void merge(@Nullable final Project project) {
        if (project.getUserId() == null || project.getUserId().isEmpty()) return;
        if (project.getId() == null || project.getId().isEmpty()) return;
        if (project.getName() == null || project.getName().isEmpty()) return;
        if (project.getDescription() == null || project.getDescription().isEmpty()) return;
        projectRepository.merge(project);
    }

    @Nullable
    @Override
    public Collection<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findAll(userId);
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findOne(userId, id);
    }

}
