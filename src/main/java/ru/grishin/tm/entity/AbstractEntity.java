package ru.grishin.tm.entity;

import org.jetbrains.annotations.Nullable;

public abstract class AbstractEntity<T extends AbstractEntity> {

    @Nullable
    protected String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
