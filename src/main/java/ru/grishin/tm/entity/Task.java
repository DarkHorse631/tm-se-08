package ru.grishin.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.sonatype.inject.Nullable;
import ru.grishin.tm.util.DateUtil;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractEntity {

    @Nullable
    private String projectId;
    @Nullable
    private String userId;
    @Nullable
    private String name;
    @Nullable
    private String description;
    @Nullable
    private Date dateStart;
    @Nullable
    private Date dateFinish;

    @Override
    public String toString() {
        return "Project ID = [" + projectId +
                "], User ID = [" + userId +
                "], Task ID = [" + id +
                "], Task name = [" + name +
                "], Description =[" + description +
                "], Start date = [" + DateUtil.FORMAT_DATE(dateStart) +
                "], Completion date =[" + DateUtil.FORMAT_DATE(dateFinish) + "]";
    }

}
