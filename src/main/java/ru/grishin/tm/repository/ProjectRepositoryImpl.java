package ru.grishin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.api.repository.ProjectRepository;
import ru.grishin.tm.entity.AbstractEntity;
import ru.grishin.tm.entity.Project;

import java.util.*;

public final class ProjectRepositoryImpl extends AbstractRepository<Project> implements ProjectRepository {

    @Override
    public void merge(@NotNull final AbstractEntity entity) {
        final Project project = (Project) entity;
        if (entities.containsKey(entity.getId()))
            update(project.getUserId(), project.getId(), project.getName(), project.getDescription(), project.getDateStart(), project.getDateFinish());
        else
            insert(project.getId(), project.getUserId(), project.getName(), project.getDescription(), project.getDateStart(), project.getDateFinish());
    }

    @Override
    public void insert(@NotNull final String id, @NotNull final String userId, @NotNull final String name, @NotNull final String description, @NotNull final Date dateStart, @NotNull final Date dateFinish) {
        final Project project = new Project();
        project.setId(id);
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(dateStart);
        project.setDateFinish(dateFinish);
        entities.put(id, project);
    }

    public void remove(@NotNull final String userId, @NotNull final String id) {
        final Project project = entities.get(id);
        if (project == null) return;
        if (project.getUserId().equals(userId)) entities.remove(id);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final String id, @NotNull final String name, @NotNull final String description, @NotNull final Date startDate, @NotNull final Date completionDate) {
        final Project project = entities.get(id);
        if (!project.getUserId().equals(userId)) return;
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(startDate);
        project.setDateFinish(completionDate);
        entities.put(id, project);

    }

    @Override
    public Collection<Project> findAll(@NotNull final String userId) {
        final Collection<Project> result = new LinkedList<>();
        Iterator<Map.Entry<String, Project>> entryIterator = entities.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Project> object = entryIterator.next();
            if (object.getValue().getUserId().equals(userId)) result.add(object.getValue());
        }
        return result;
    }

    public Project findOne(@NotNull final String userId, @NotNull final String id) {
        final Project project = entities.get(id);
        if (project == null) return null;
        if (project.getUserId().equals(userId)) return project;
        return null;
    }
}
