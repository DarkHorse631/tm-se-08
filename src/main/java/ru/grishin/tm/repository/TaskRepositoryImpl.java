package ru.grishin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.api.repository.TaskRepository;
import ru.grishin.tm.entity.AbstractEntity;
import ru.grishin.tm.entity.Task;

import java.util.*;

public final class TaskRepositoryImpl extends AbstractRepository<Task> implements TaskRepository {
    @Override
    public void merge(@NotNull final AbstractEntity entity) {
        final Task task = (Task) entity;
        if (entities.containsKey(task.getId()))
            update(task.getUserId(), task.getId(), task.getName(), task.getDescription(), task.getDateStart(), task.getDateFinish());
        else
            insert(task.getProjectId(), task.getUserId(), task.getId(), task.getName(), task.getDescription(), task.getDateStart(), task.getDateFinish());
    }


    @Override
    public void insert(@NotNull final String projectId, @NotNull final String userId, @NotNull final String id, @NotNull final String name, @NotNull final String description, @NotNull final Date dateStart, @NotNull final Date dateFinish) {
        final Task task = new Task();
        task.setProjectId(projectId);
        task.setUserId(userId);
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(dateStart);
        task.setDateFinish(dateFinish);
        entities.put(id, task);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String id) {
        final Task task = entities.get(id);
        if (task == null) return;
        if (task.getUserId().equals(userId)) entities.remove(id);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final String id, @NotNull final String name, @NotNull final String description, @NotNull final Date dateStart, @NotNull final Date dateFinish) {
        final Task task = entities.get(id);
        if (!task.getUserId().equals(userId)) return;
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(dateStart);
        task.setDateFinish(dateFinish);
        entities.put(task.getId(), task);
    }

    @Override
    public Collection<Task> findAll(@NotNull final String userId) {
        final Collection<Task> result = new LinkedList<>();
        Iterator<Map.Entry<String, Task>> entryIterator = entities.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Task> object = entryIterator.next();
            if (object.getValue().getUserId().equals(userId)) result.add(object.getValue());
        }
        return result;
    }

    @Nullable
    @Override
    public Task findOne(@NotNull final String userId, @NotNull final String id) {
        final Task task = entities.get(id);
        if (task == null) return null;
        if (task.getUserId().equals(userId)) return task;
        return null;
    }

    @Override
    public void deleteByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        final Iterator<Map.Entry<String, Task>> entryIterator = entities.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Task> object = entryIterator.next();
            if (!object.getValue().getUserId().equals(userId)) continue;
            if (object.getValue().getProjectId().equals(projectId)) entryIterator.remove();
        }
    }
}
