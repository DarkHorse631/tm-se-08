package ru.grishin.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;

import ru.grishin.tm.enumerate.RoleType;

public final class AboutCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Information about program.";
    }

    @Override
    public void execute() throws Exception {
        BasicConfigurator.configure();
        System.out.println("Developer: " + Manifests.read("Developer"));
        System.out.println("Version: " + Manifests.read("Version"));
        System.out.println("BuildNumber (" + Manifests.read("BuildNumber") + ")");
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ANONYMOUS_USER, RoleType.USER, RoleType.ADMIN};
    }
}
