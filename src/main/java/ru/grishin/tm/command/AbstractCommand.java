package ru.grishin.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.api.service.ServiceLocator;
import ru.grishin.tm.enumerate.RoleType;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;

    @NotNull
    public abstract RoleType [] roles();

}
