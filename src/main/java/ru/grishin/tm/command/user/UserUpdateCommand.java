package ru.grishin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

public final class UserUpdateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "uu";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update current user.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Update current user--");
        System.out.print("Enter new login: ");
        final String login = serviceLocator.getScanner().nextLine();
        System.out.print("Enter new password: ");
        final String pass1 = serviceLocator.getScanner().nextLine();
        System.out.print("Confirm your password: ");
        final String pass2 = serviceLocator.getScanner().nextLine();
        if (pass1.equals(pass2)) {
            serviceLocator.getUserService().update(serviceLocator.getCurrentUser().getId(), login, pass1, RoleType.USER);
            System.out.println("[USER UPDATED]");
        } else {
            System.out.println("Passwords don't match!");
            execute();
        }
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
