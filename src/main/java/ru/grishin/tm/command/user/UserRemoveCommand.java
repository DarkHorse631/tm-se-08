package ru.grishin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

public final class UserRemoveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "ud";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Delete user.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--DELETE USER--");
        System.out.print("Enter user id: ");
        final String id = serviceLocator.getScanner().nextLine();
        serviceLocator.getUserService().remove(id);
        System.out.println("[USER [" + id + "] DELETED]");
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ADMIN};
    }
}
