package ru.grishin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.entity.Task;

import java.util.Collection;
import java.util.Date;

public interface TaskRepository extends Repository {

    void insert(@NotNull String projectId, @NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description, @NotNull Date dateStart, @NotNull Date dateFinish);

    void remove(@NotNull String userId, @NotNull String id);

    void update(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description, @NotNull Date dateStart, @NotNull Date dateFinish);

    @Nullable
    Collection<Task> findAll(@NotNull String userId);

    @Nullable
    Task findOne(@NotNull String userId, @NotNull String id);

    void deleteByProjectId(@NotNull String userId, @NotNull String projectId);

}
