package ru.grishin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.entity.Project;

import java.util.Collection;
import java.util.Date;

public interface ProjectRepository extends Repository {

    void insert(@NotNull String id, @NotNull String userId, @NotNull String name, @NotNull String description, @NotNull Date startDate, @NotNull Date completionDate);

    void remove(@NotNull String userId, @NotNull String id);

    void update(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description, @NotNull Date startDate, @NotNull Date completionDate);

    Collection<Project> findAll(@NotNull String userId);

    Project findOne(@NotNull String userId, @NotNull String id);
}
