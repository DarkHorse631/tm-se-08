package ru.grishin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.entity.AbstractEntity;

public interface Repository<T extends AbstractEntity> {

    void persist(@NotNull T t);

    void merge(@NotNull T t);

    boolean isExist(@NotNull String id);
}
