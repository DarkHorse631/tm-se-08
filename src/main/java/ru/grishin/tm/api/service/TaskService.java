package ru.grishin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.entity.Task;

import java.util.Date;

public interface TaskService extends Service {

    void create(@Nullable String projectId, @Nullable String userId, @Nullable String name, @Nullable String description, @Nullable Date startDate, @Nullable Date completionDate);

    void update(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description, @Nullable Date startDate, @Nullable Date completionDate);

    void merge(@Nullable Task task);

    void deleteByProjectId(@Nullable String userId, @Nullable String projectId);
}
