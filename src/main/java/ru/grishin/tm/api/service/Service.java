package ru.grishin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.entity.AbstractEntity;

import java.util.Collection;

public interface Service<T extends AbstractEntity> {

    void remove(@Nullable String userId, @Nullable String id);

    @Nullable
    Collection<T> findAll(@Nullable String userId);

    @Nullable
    T findOne(@Nullable String userId, @Nullable String id);
}
