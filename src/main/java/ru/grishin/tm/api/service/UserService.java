package ru.grishin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.entity.User;
import ru.grishin.tm.enumerate.RoleType;

import java.util.Collection;

public interface UserService {

    void registryUser(@Nullable String login, @Nullable String password);

    void registryAdmin(@Nullable String login, @Nullable String password);

    @Nullable
    User login(@Nullable String login, @Nullable String password);

    void remove(@Nullable String id);

    void update(@Nullable String id, @Nullable String login, @Nullable String password, @Nullable RoleType roleType);

    void updatePassword(@Nullable String id, @Nullable String currentPassword, @Nullable String newPassword);

    void merge(@Nullable User user);

    @Nullable
    Collection<User> findAll();

    @Nullable
    User findOne(@Nullable String Id);
}
