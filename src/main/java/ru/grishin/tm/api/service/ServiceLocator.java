package ru.grishin.tm.api.service;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.entity.User;


import java.util.Collection;
import java.util.Scanner;

public interface ServiceLocator {
    @NotNull
    ProjectService getProjectService();

    @NotNull
    TaskService getTaskService();

    @NotNull
    UserService getUserService();

    @NotNull
    Scanner getScanner();

    void setCurrentUser(User currentUser);

    @Nullable
    User getCurrentUser();

    @NotNull
    Collection<AbstractCommand> getCommands();
}
