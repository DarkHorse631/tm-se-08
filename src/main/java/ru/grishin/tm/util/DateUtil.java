package ru.grishin.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@UtilityClass
public final class DateUtil {
    @Nullable
    public static String FORMAT_DATE(@Nullable final Date date) {
        if (date == null) return null;
        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        String result = null;
        return result = dateFormat.format(date);
    }

    @Nullable
    public static Date PARSE_DATE(@Nullable final String date) {
        if (date == null || date.isEmpty()) return null;
        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date result = null;
        try {
            result = dateFormat.parse(date);
        } catch (ParseException e) {
            System.out.println("Date is null");
        }
        return result;
    }
}
